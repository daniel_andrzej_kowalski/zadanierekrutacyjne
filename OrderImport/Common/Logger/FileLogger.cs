﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Logger
{
    public class FileLogger
    {
        private readonly string _filePath;

        public FileLogger()
        {
            var directoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Rekturacja", "OrderImport");
            _filePath = Path.Combine(directoryPath, "Log.txt");
            if (!File.Exists(_filePath))
            {
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                var file = File.Create(_filePath);
                file.Close();
            }
        }

        public void LogToFile(string message)
        {
            using (var file = new FileStream(_filePath, FileMode.Append))
            using (var writer = new StreamWriter(file, Encoding.UTF8))
            {
                writer.WriteLine($"[{DateTime.Now:dd.MM.yyyy hh:mm:ss}] {message}");
            }

        }
    }
}
