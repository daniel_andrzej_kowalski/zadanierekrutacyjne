﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class ProductModel
    {
        [JsonProperty("storage ")]
        public string Storage { get; set; }

        [JsonProperty("storage_id")]
        public int StorageId { get; set; }

        [JsonProperty("order_product_id")]
        public int OrderProductId { get; set; }

        [JsonProperty("product_id")]
        public string ProductId { get; set; }

        [JsonProperty("variant_id")]
        public string VariantId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("ean")]
        public string Ean { get; set; }

        [JsonProperty("auction_id")]
        public string AuctionId { get; set; }

        [JsonProperty("attributes")]
        public string Attributes { get; set; }

        [JsonProperty("price_brutto")]
        public decimal PriceBrutto { get; set; }

        [JsonProperty("tax_rate")]
        public int TaxRate { get; set; }

        [JsonProperty("quantity")]
        public int Quanity { get; set; }

        [JsonProperty("weight")]
        public float Weight { get; set; }
    }
}
