﻿using BaseLinkerAPI;
using BaseLinkerAPI.Services;
using Common.Logger;
using Newtonsoft.Json;
using SubiektGtAPI.Models;
using SubiektGtAPI.Services;
using System;
using System.Configuration;

namespace OrderImport
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            var logger = new FileLogger();

            var context = new BaseLinkerContext(ConfigurationManager.AppSettings["BaseLinkerToken"]);
            var subiektGtConfigurationString = ConfigurationManager.AppSettings["SubiektGtConfig"];

            var subiektGtConfiguration = JsonConvert.DeserializeObject<SubiektGtConfiguration>(subiektGtConfigurationString);

            var baseLinkerOrderService = new BaseLinkerOrderService(context);
            var subiektGtOrderService = new SubiektGtOrderService(subiektGtConfiguration);

            while (true)
            {
                Console.WriteLine("Podaj numer zamówienia z BaseLinker:");

                var orderIdString = Console.ReadLine();

                if (int.TryParse(orderIdString, out int orderId))
                {
                    try
                    {

                        var order = baseLinkerOrderService.GetOrder(orderId);

                        subiektGtOrderService.AddOrder(order);

                        Console.WriteLine("Dodano zamówienie.");
                        break;
                    }
                    catch (Exception ex)
                    {
                        logger.LogToFile(ex.ToString());
                        Console.WriteLine("Coś poszło nie tak.");
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Podany ciąg znaków nie jest numerem zamówienia. Powinien składać się z samych cyfr.");
                }

            }

            Console.WriteLine("Wsciścij enter, aby zakończyć.");
            Console.ReadLine();
        }
    }
}
