﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLinkerAPI.Interfaces
{
    public interface IResponseModel
    {
        string Status { get; set; }
    }
}
