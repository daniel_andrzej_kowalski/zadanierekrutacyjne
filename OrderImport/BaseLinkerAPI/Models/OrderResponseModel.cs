﻿using BaseLinkerAPI.Interfaces;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLinkerAPI.Models
{
    public class OrderResponseModel : IResponseModel
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("orders")]
        public OrderModel[] Orders { get; set; }
    }
}
