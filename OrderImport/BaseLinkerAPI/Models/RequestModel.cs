﻿using BaseLinkerAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLinkerAPI.Models
{
    public class RequestModel
    {
        public string Token { get; set; }
        public string Method { get; set; }
        public IRequestParametersModel Parameters { get; set; }
    }
}
