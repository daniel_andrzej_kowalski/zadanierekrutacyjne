﻿using BaseLinkerAPI.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLinkerAPI.Models
{
    public class OrderRequestParametersModel : IRequestParametersModel
    {
        [JsonProperty("order_id")]
        public int? OrderId { get; set; }

        [JsonProperty("date_confirmed_from")]
        public int? DateConfirmedFrom { get; set; }

        [JsonProperty("date_from")]
        public int? DateFrom { get; set; }

        [JsonProperty("id_from")]
        public int? IdFrom { get; set; }

        [JsonProperty("get_unconfirmed_orders")]
        public bool GetUnconfirmedOrders { get; set; } = false;

        [JsonProperty("status_id")]
        public int? StatusId { get; set; }

        [JsonProperty("filter_email")]
        public string FiletEmail { get; set; }
    }
}
