﻿using BaseLinkerAPI.Interfaces;
using BaseLinkerAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BaseLinkerAPI
{
    public class BaseLinkerContext
    {
        private readonly string _token;

        public BaseLinkerContext(string token)
        {
            _token = token;
        }

        public R DoRequest<P, R>(string method, P requestParameters) 
            where P : IRequestParametersModel 
            where R : IResponseModel
        {
            var contentBody = new Dictionary<string, string>
            {
                { "method", method },
                { "token", _token },
                { "parameters", JsonConvert.SerializeObject(requestParameters) }
            };

            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(contentBody);

                var response = client.PostAsync("https://api.baselinker.com/connector.php", content).Result;

                response.EnsureSuccessStatusCode();

                var responseString = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<R>(responseString);
            }
        }
    }
}
