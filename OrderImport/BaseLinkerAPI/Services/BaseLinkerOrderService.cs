﻿using BaseLinkerAPI.Interfaces;
using BaseLinkerAPI.Models;
using Common.Exceptions;
using Common.Models;
using System;
using System.Linq;

namespace BaseLinkerAPI.Services
{
    public class BaseLinkerOrderService : AbstractBaseLinkerService, IBaseLinkerOrderService
    {
        public BaseLinkerOrderService(BaseLinkerContext context) : base(context)
        {
        }

        public OrderModel GetOrder(int orderId)
        {
            var ordersResponse = Context.DoRequest<OrderRequestParametersModel, OrderResponseModel>(
            "getOrders", new OrderRequestParametersModel
            {
                OrderId = orderId,
                GetUnconfirmedOrders = true
            });

            if (ordersResponse.Orders.Any())
            {
                return ordersResponse.Orders[0];
            }

            throw new NotFoundException($"zamówienia o id {orderId}");
        }
    }
}
