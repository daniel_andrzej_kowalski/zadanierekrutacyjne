﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLinkerAPI.Services
{
    public abstract class AbstractBaseLinkerService
    {
        protected BaseLinkerContext Context { get; }

        public AbstractBaseLinkerService(BaseLinkerContext context)
        {
            Context = context;
        }
    }
}
