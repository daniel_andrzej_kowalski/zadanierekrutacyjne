﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubiektGtAPI.Interfaces
{
    public interface ISubiektGtOrderService
    {
        void AddOrder(OrderModel order);
    }
}
