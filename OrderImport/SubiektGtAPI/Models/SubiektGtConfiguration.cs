﻿namespace SubiektGtAPI.Models
{
    public class SubiektGtConfiguration
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Operator { get; set; }
        public string OperatorPassword { get; set; }
    }
}
