﻿using ADODB;
using Common.Models;
using InsERT;
using SubiektGtAPI.Interfaces;
using SubiektGtAPI.Models;
using System;

namespace SubiektGtAPI.Services
{
    public class SubiektGtOrderService : ISubiektGtOrderService
    {
        private readonly SubiektGtConfiguration _config;

        public SubiektGtOrderService (SubiektGtConfiguration config)
        {
            _config = config;
        }

        public void AddOrder(OrderModel orderModel)
        {
            var gt = new GT
            {
                Produkt = ProduktEnum.gtaProduktSubiekt,
                Serwer = _config.Server,
                Baza = _config.Database,
                Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                Uzytkownik = _config.User,
                UzytkownikHaslo = _config.Password,
                Operator = _config.Operator,
                OperatorHaslo = _config.OperatorPassword
            };

            var subiekt = (Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj, (int)UruchomEnum.gtaUruchomWTle);

            var rs = new Recordset();

            var sql = $"select top(1) pa_Id from sl_Panstwo where pa_KodPanstwaISO = '{orderModel.InvoiceCountryCode}';";

            rs.Open(sql, gt.Polaczenie, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockBatchOptimistic, 0);

            if (rs.Fields.Count == 0)
            {
                throw new Exception($"Nie znaleziono kraju dla kodu {orderModel.InvoiceCountryCode}");
            }
            var countryId = rs.Fields[0].Value;
 

            var documents = subiekt.SuDokumentyManager;
            var contractors = subiekt.KontrahenciManager;

            var order = documents.DodajZK();

            if (!string.IsNullOrEmpty(orderModel.InvoiceNip) && contractors.IstniejeWg(orderModel.InvoiceNip, KontrahentParamWyszukEnum.gtaKontrahentWgNip))
            {
                var contractor = contractors.Wczytaj(orderModel.InvoiceNip);

                order.KontrahentId = contractor.Identyfikator;

                contractor.Zamknij();
            }
            else
            {
                var contractor = contractors.DodajKontrahentaJednorazowego();

                var fullName = string.IsNullOrEmpty(orderModel.InvoiceFullName) ? orderModel.InvoiceCompany : orderModel.InvoiceFullName;

                contractor.Email = orderModel.Email;
                contractor.KodPocztowy = orderModel.InvoicePostCode;
                contractor.Miejscowosc = orderModel.InvoiceCity;
                contractor.Nazwa = fullName.Length > 50 ? fullName.Substring(0, 50) : fullName;
                contractor.NazwaPelna = fullName;
                contractor.NIP = orderModel.InvoiceNip;
                contractor.Ulica = orderModel.InvoiceAddress;
                contractor.Panstwo = countryId;
                contractor.Telefony.Dodaj(orderModel.Phone);

                contractor.Zapisz();

                order.KontrahentId = contractor.Identyfikator;

                contractor.Zamknij();
            }

            foreach (var prod in orderModel.Products)
            {
                var product = order.Pozycje.DodajUslugeJednorazowa();
                product.CenaBruttoPrzedRabatem = prod.PriceBrutto;
                product.IloscJm = prod.Quanity;
                product.UslJednNazwa = prod.Name;
            }

            order.Zapisz();
        }
    }
}
